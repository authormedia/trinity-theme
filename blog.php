<?php
/*
Template Name: Blog Page
*/
?>

<?php get_header(); ?>

	<div class="content">

		<header class="page-header">
			<h2 class="page-title">Blog</h2>
		</header>

		<?php
			global $wp_query;
			$wp_query = new WP_Query(array('orderby' => 'post_date','order' => 'DESC', 'post_type' => 'post', 'post_status' => 'publish', 'paged'=> (get_query_var('paged') ? get_query_var('paged') : 1), 'posts_per_page' => 10));
		?>

		<?php if(have_posts()) { ?>

			<?php
				show_pagination('pagination-above');
				while(have_posts())
				{
					the_post();
					get_template_part('excerpt', get_post_type());
				}
				show_pagination('pagination-below');
			?>

		<?php } else { ?>

			<?php get_template_part('nothing-found'); ?>

		<?php } ?>

	</div>

	<?php get_sidebar(); ?>

<?php get_footer(); ?>