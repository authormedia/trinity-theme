<?php get_header(); ?>

		<div class="content">
			<?php the_post(); ?>

			<header class="entry-header">
				<h2 class="entry-title"><?php the_title(); ?></h2>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php the_content(); ?>
				<?php edit_post_link('Edit', '<span class="edit-link">', '</span>'); ?>
			</div>
		</div>

		<?php get_sidebar(); ?>

<?php get_footer(); ?>