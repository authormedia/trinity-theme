<div class="comments">

	<?php if(post_password_required()): ?>

		<p class="nopassword">This post is password protected. Enter the password to view any comments.</p>

	<?php else: ?>

		<?php if(have_comments()): ?>

			<h2 class="comments-title">
				<?php
					echo((get_comments_number() > 1 ? get_comments_number().' thoughts' : 'One thought') . ' on &ldquo;'.get_the_title().'&rdquo;');
				?>
			</h2>

			<?php //UPPER COMMENT NAVIGATION ?>
			<?php if(get_comment_pages_count() > 1 && get_option( 'page_comments')): ?>
			<nav class="comment-pagination">
				<div class="previous"><?php previous_comments_link('&larr; Older Comments'); ?></div>
				<div class="next"><?php next_comments_link('Newer Comments &rarr;'); ?></div>
			</nav>
			<?php endif; ?>

			<?php //ACTUAL COMMENTS ?>
			<ol class="commentlist">
				<?php wp_list_comments(); ?>
			</ol>

			<?php //LOWER COMMENT NAVIGATION ?>
			<?php if(get_comment_pages_count() > 1 && get_option( 'page_comments')): ?>
			<nav class="comment-pagination">
				<div class="previous"><?php previous_comments_link('&larr; Older Comments'); ?></div>
				<div class="next"><?php next_comments_link('Newer Comments &rarr;'); ?></div>
			</nav>
			<?php endif; ?>

		<?php elseif(!comments_open() and !is_page() and post_type_supports(get_post_type(), 'comments')): ?>

			<p class="nocomments">Comments are closed.</p>
			
		<?php endif; ?>

		<?php comment_form(array('title_reply' => '<span class="title_reply">Leave a Comment</span><span class="title_reply_to">Leave a Reply</span>')); ?>

	<?php endif; ?>

</div>
