<article class="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h2 class="entry-title"><?php the_title(); ?></h1>
		<div class="entry-meta">
			<p class="meta-top">by <?php the_author_posts_link(); ?>, On <?php echo(get_the_date()); ?> in <span class="cat-link"><?php the_category(', '); ?></span></p>
			<p class="meta-btm">
				<?php if(has_tag()): ?><span class="tags"><?php the_tags('Tagged in: ', ', '); ?></span> | <?php endif; ?>
				<a href="<?php comments_link(); ?>"><?php if(get_comments_number()==0){echo('No Comments');}elseif(get_comments_number() > 1){echo(get_comments_number().' Comments');}else{echo("1 Comment");}?></a> 
				<?php edit_post_link('Edit', '<span class="edit-link"> ', '</span>'); ?>
			</p>
		</div>
	</header>

	<div class="entry-content">
		<div class="feature-img"><?php the_post_thumbnail('thumbnail'); ?></div>
		<?php the_excerpt(); ?>
		<div class="read-button"><a href="<?php the_permalink(); ?>">Read More &raquo;</a></div>
		<hr />
	</div>
</article>