<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo(is_mobile() ? get_bloginfo('stylesheet_directory').'/mobile.css' : get_bloginfo('stylesheet_url')); ?>" />
<meta name="viewport" content="width=device-width, <?php echo(is_mobile() ? 'initial-scale=0.25' : ''); ?>" />
<title><?php wp_title(); ?></title>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="page">
	<header class="site-header">
		<h1 class="site-title"><a href="<?php echo(home_url()); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
		<h2 class="site-description"><?php bloginfo('description'); ?></h2>
		<?php if(has_nav_menu("main-menu")) { wp_nav_menu(array('theme_location' => 'main-menu', 'container' => 'nav', 'container_class' => 'site-menu')); } ?>
	</header>

	<div class="main">