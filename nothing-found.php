<article class="post no-results not-found">
	<header class="entry-header">
		<h1 class="entry-title">Nothing Found</h1>
	</header>
	<div class="entry-content">
		<p>Sorry, nothing here. Perhaps searching will help find something related.</p>
		<?php get_search_form(); ?>
	</div>
</article>