<?php get_header(); ?>

	<div class="content">

		<article class="post error404 not-found">
			<header class="entry-header">
				<h1 class="entry-title">404 Error: Page Not Found</h1>
			</header>
			<div class="entry-content">
				<p>It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching or one of the links in the sidebar can help.</p>
				<?php get_search_form(); ?>
			</div>
		</article>

	</div>
			
	<?php get_sidebar(); ?>

<?php get_footer(); ?>