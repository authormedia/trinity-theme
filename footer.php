		<div style="clear:both;"></div>
	</div>

	<footer class="site-footer">
		<div class="site-signature">
			<?php echo(apply_filters('site-signature', '<a href="http://www.castlemediagroup.com/">Built by Castle Media Group</a>')); ?>
		</div>
	</footer>

</div>

<?php wp_footer(); ?>

</body>
</html>