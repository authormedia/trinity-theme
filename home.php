<?php
/*
Home Page Template
*/

get_header(); ?>

	<div class="content">

		<?php if(have_posts()) { ?>

			<?php
				show_pagination('pagination-above');
				while(have_posts())
				{
					the_post();
					get_template_part('excerpt', get_post_type());
				}
				show_pagination('pagination-below');
			?>

		<?php } else { ?>

			<?php get_template_part('nothing-found'); ?>

		<?php } ?>

	</div>
	
	<?php get_sidebar(); ?>

<?php get_footer(); ?>