<?php

//Detects android devices
function is_mobile()
{
	global $is_iphone;
	return $is_iphone || preg_match('/android/i', $_SERVER['HTTP_USER_AGENT']);
}

//Display navigation to next/previous pages if the query has multiple pages
function show_pagination($class="")
{
	echo(get_pagination($class));
}
function get_pagination($class="")
{
	global $wp_query;
	if($wp_query->max_num_pages <= 1){return "";}

	$output  = '<nav class="'.$class.'">';
	$output .= '<div class="previous">'.get_next_posts_link(apply_filters('pagination_older', '&larr; Older posts')).'</div>';
	$output .= '<div class="next">'.get_previous_posts_link(apply_filters('pagination_newer', 'Newer posts &rarr;')).'</div>';
	$output .= '</nav>';
	return apply_filters("trinity_pagination", $output);
}

if(!function_exists('trinity_setup'))
{
	function trinity_setup()
	{
		//Register the main sidebar
		register_sidebar(array('name' =>'Main Sidebar', 'class' => 'main-sidebar',
		'description' => 'The main sidebar widget area',
		'before_widget' => '<aside class="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>'));

		// Register the main navigation menu
		register_nav_menu('main-menu', 'Main Menu');

		// This theme uses thumbnails
		add_theme_support('post-thumbnails');
		set_post_thumbnail_size(89, 89, true);
	}
}
add_action('after_setup_theme', 'trinity_setup');

//Sets the post excerpt length to 75 words.
/*
function custom_override_excerpt_length($length){return 75;}
add_filter('excerpt_length', 'custom_override_excerpt_length');
*/

//Replaces the "[...]" appended to automatically generated excerpts with an HTML ellipsis
/*
function custom_override_excerpt_more($more){return '&hellip; <a href="'.get_permalink().'">Read more</a>';}
add_filter('excerpt_more', 'custom_override_excerpt_more');
*/

//Add IE8 and IE9 support via styles
function custom_override_language_attributes($output)
{
	global $is_IE;
	if($is_IE)
	{
		if(strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 8'))
		{
			$output .= ' class="ie8" ';
		}
		elseif(strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 9'))
		{
			$output .= ' class="ie9" ';
		}
	}
	return $output;
}
add_filter('language_attributes', 'custom_override_language_attributes');

//Add IE8 support via scripts
function add_html5shiv_script()
{
?>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php
/*<script src="<?php echo(get_template_directory_uri()); ?>/js/html5.js" type="text/javascript"></script>*/
}
add_action('wp_head', 'add_html5shiv_script');

//Add support for pingbacks
function add_support_for_pingbacks()
{
	?> <link rel="pingback" href="<?php get_bloginfo('pingback_url'); ?>" /> <?php
}
add_action('wp_head', 'add_support_for_pingbacks');

//Add comment reply javascript
function add_comment_reply_js()
{
	wp_deregister_script('comment-reply');
	wp_register_script('comment-reply', get_template_directory_uri()."/comment-reply.js");
	if(is_singular() && get_option('thread_comments'))
	{
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_head', 'add_comment_reply_js');