
addComment = {
	moveForm : function(commId, parentId, respondId, postId) {
		var t = this, div, comm = t.I(commId), respond = t.I(respondId), cancel = t.I('cancel-comment-reply-link'), parent = t.I('comment_parent'), post = t.I('comment_post_ID');
		var title_reply = t.I('title_reply'), title_reply_to = t.I('title_reply_to');

		if ( ! comm || ! respond || ! cancel || ! parent || ! title_reply || ! title_reply_to )
			return;

		t.respondId = respondId;
		postId = postId || false;

		if ( ! t.I('wp-temp-form-div') ) {
			div = document.createElement('div');
			div.id = 'wp-temp-form-div';
			div.style.display = 'none';
			respond.parentNode.insertBefore(div, respond);
		}

		comm.parentNode.insertBefore(respond, comm.nextSibling);
		if ( post && postId )
			post.value = postId;
		parent.value = parentId;
		cancel.style.display = '';
		title_reply.style.display = 'none';
		title_reply_to.style.display = 'inline';

		cancel.onclick = function() {
			var t = addComment, temp = t.I('wp-temp-form-div'), respond = t.I(t.respondId);
			var title_reply = t.I('title_reply'), title_reply_to = t.I('title_reply_to');

			if ( ! temp || ! respond || ! title_reply || ! title_reply_to )
				return;

			t.I('comment_parent').value = '0';
			temp.parentNode.insertBefore(respond, temp);
			temp.parentNode.removeChild(temp);
			this.style.display = 'none';
			title_reply.style.display = 'inline';
			title_reply_to.style.display = 'none';
			this.onclick = null;
			return false;
		}

		try { t.I('comment').focus(); }
		catch(e) {}

		return false;
	},

	I : function(e) {
		return document.getElementById(e);
	}
}
