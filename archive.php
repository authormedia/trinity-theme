<?php get_header(); ?>

	<div class="content">

		<header class="page-header">
			<h1 class="page-title">
				<?php
					if(is_category())
					{
						echo("Category Archives");
					}
					elseif(is_author())
					{
						echo("Author Archives");
					}
					elseif(is_day())
					{
						echo("Daily Archives: ".get_the_date());
					}
					elseif(is_month())
					{
						echo("Monthly Archives: ".get_the_date('F Y'));
					}
					elseif(is_year())
					{
						echo("Yearly Archives: ".get_the_date('Y'));
					}
					elseif(is_search())
					{
						echo("Search Results for: ".get_search_query());
					}
					else
					{
						echo("Archives");
					}
				?>
			</h1>
		</header>

		<?php if(have_posts()) { ?>

			<?php
				show_pagination('pagination-above');
				while(have_posts())
				{
					the_post();
					get_template_part('excerpt', get_post_type());
				}
				show_pagination('pagination-below');
			?>

		<?php } else { ?>

			<?php get_template_part('nothing-found'); ?>

		<?php } ?>

	</div>
	
	<?php get_sidebar(); ?>

<?php get_footer(); ?>